Run application
```
docker-compose up -d
```
Build
```
docker-compose run --rm --no-deps php-fpm sh -lc 'composer install'
```
Build migrations
```
docker-compose exec php-fpm php bin/console doctrine:migrations:migrate
```

App available on
```
http://localhost:11204/
```