<?php

namespace App\Repository;

use App\Entity\EmployeeDossier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class EmployeeDossierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmployeeDossier::class);
    }

    public function save(EmployeeDossier $employeeDossier): void
    {
        $this->getEntityManager()->persist($employeeDossier);
        $this->getEntityManager()->flush();
    }
}
