<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardRepository")
 * @ORM\Table(name="card")
 */
class Card
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     *
     * @Assert\Luhn(message="Invalid card number.")
     */
    private $number;

    /**
     * @ORM\Column(type="smallint")
     *
     * @Assert\Length(min="4", max="4")
     */
    private $cvv;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function getCvv(): ?int
    {
        return $this->cvv;
    }

    public function setNumber($number): void
    {
        $this->number = $number;
    }

    public function setCvv($cvv): void
    {
        $this->cvv = $cvv;
    }
}
