<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\EmployeeDossier;
use App\Form\EmployeeDossierType;
use App\Repository\EmployeeDossierRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/employee-dossiers")
 */
class EmployeeDossierController extends AbstractController
{
    /**
     * @Route("/", name="employee_dossier_index", methods={"GET"})
     *
     * @param EmployeeDossierRepository $employeeDossierRepository
     *
     * @return Response
     */
    public function index(EmployeeDossierRepository $employeeDossierRepository): Response
    {
        return $this->render('employee_dossier/index.html.twig', [
            'employee_dossiers' => $employeeDossierRepository->findAll(),
        ]);
    }

    /**
     * @Route("/create", name="employee_dossier_create", methods={"GET","POST"})
     *
     * @param Request                   $request
     * @param EmployeeDossierRepository $repository
     *
     * @return Response
     */
    public function create(Request $request, EmployeeDossierRepository $repository): Response
    {
        $employeeDossier = new EmployeeDossier();
        $form = $this->createForm(EmployeeDossierType::class, $employeeDossier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repository->save($employeeDossier);

            $this->addFlash('success', sprintf(
                'Dossier "%s" successfully created',
                $employeeDossier->getNumber()
            ));

            return $this->redirectToRoute('employee_dossier_index');
        }

        return $this->render('employee_dossier/create.html.twig', [
            'employee_dossier' => $employeeDossier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="employee_dossier_edit", methods={"GET","POST"}, requirements={"id"="\d+"})
     *
     * @param Request         $request
     * @param EmployeeDossier $employeeDossier
     *
     * @return Response
     */
    public function edit(Request $request, EmployeeDossier $employeeDossier): Response
    {
        $form = $this->createForm(EmployeeDossierType::class, $employeeDossier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', sprintf(
                'Dossier "%s" successfully updated',
                $employeeDossier->getNumber()
            ));

            return $this->redirectToRoute('employee_dossier_index');
        }

        return $this->render('employee_dossier/edit.html.twig', [
            'employee_dossier' => $employeeDossier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="employee_dossier_delete", methods={"GET", "DELETE"}, requirements={"id"="\d+"})
     *
     * @param Request         $request
     * @param EmployeeDossier $employeeDossier
     *
     * @return Response
     */
    public function delete(Request $request, EmployeeDossier $employeeDossier): Response
    {
        if ($request->getMethod() === 'DELETE') {
            $tokenId = sprintf('delete%s', $employeeDossier->getId());
            if ($this->isCsrfTokenValid($tokenId, $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($employeeDossier);
                $entityManager->flush();

                $this->addFlash('success', sprintf(
                    'Dossier "%s" successfully deleted!',
                    $employeeDossier->getNumber()
                ));

                return $this->redirectToRoute('employee_dossier_index');
            }
        }

        return $this->render('employee_dossier/delete.html.twig', [
            'employee_dossier' => $employeeDossier,
        ]);
    }
}
